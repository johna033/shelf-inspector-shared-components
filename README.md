# README #

Based on the original paper published by Kirt Lillywhite (which lacks any explanation of why is his algorithm designed the way it is designed), this is an attempt to create an object recognition algorithm, which will construct its own features for recognition, through the methodology of the GAs. Segmentation, and invariations towards space-time translations, will follow, shall we succeed 
in the implementation of the basic approach, outlined by Lillywhite. Here is a short timeline:
 08/14/15 - demo project, which appears to be working even with the shitty dataset.  

# DEPENDENCIES #

Our project has one dependency: EgmuCV 2.4.10.1940, you can dowload it from here: http://sourceforge.net/projects/emgucv/?source=typ_redirect
In order to run stuff using this lib, you will commonly need Egmu.CV.dll, Egmu.CV.GPU.dll, Egmu.CV.UI.dll, Egmu.Util.dll, which can be found in PATH_TO_EGMU\bin (you can add them as project dependencies in Visual Studio),
also you will need ZedGraph.dll, same folder just copy it to the .exe loaction, also, folders x86 and x64 need to be copied to the exe location from PATH_TO_EGMU\bin.